#!/usr/bin/env bash

set -x
set -eo pipefail

flutter build linux --release

deploy_path="$HOME/.flutter-bin/insights-client"

if [ ! -d "$deploy_path" ]; then
    mkdir -p $deploy_path
fi

cp -rf build/linux/x64/release/bundle/data $deploy_path
cp -rf build/linux/x64/release/bundle/lib $deploy_path

cp -f build/linux/x64/release/bundle/insights_client $deploy_path
