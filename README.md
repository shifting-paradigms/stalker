# Insights Client

A cross-platform telemetry dashboard to visualize your [insights](https://gitlab.com/shifting-paradigms/insights-server).

Works well in conjunction with:
- [insights-server](https://gitlab.com/shifting-paradigms/insights-server) - A light-weight, minimalistic and pragmatic telemetry server.
- [jump-start](https://gitlab.com/shifting-paradigms/jump-start) - A starter template for observable high-performance web services.

## Features
See [CHANGELOG.md](CHANGELOG.md) for a list of features.

## Getting Started

```bash
# 1. create .env file
# 2. build app
flutter build linux

# 3. run binary
./build/linux/x64/release/bundle/insights_client
```

## Performance Considerations
Currently the log dashboard is not optimized for performance.
It works well for a few hundred log entries, but will become sluggish for thousands of entries.
This is due to the fact that the entire log is rendered and displayed.
If the log list grows too large, it will be necessary to implement a virtualized list,
which only renders a subset of the log entries.
