# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2023-03-15
### Fixed
- Change placeholder icon to a more appropriate one

## [1.0.0] - 2023-02-27
### Added
- Add support for [insights-server v1.8.3](https://gitlab.com/shifting-paradigms/insights-server/-/tree/1.8.3)
- Style an animate host metrics dashboard
- Style api metrics dashboard
- Style and animate log dashboard
- Add filter to log dashboard
- Add persistent backend settings
- Support multiple backends
