import 'package:flutter/material.dart';
import 'dart:ui';

import 'app.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Insights Client',
      theme: ThemeData.dark(),
      scrollBehavior: CustomScrollBehavior(),
      home: const App(),
    ),
  );
}

// scrollbehavior
class CustomScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}
