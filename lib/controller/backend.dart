import 'dart:collection';

import 'package:insights_client/models/backend.dart';

class BackendController {
  BackendController({required this.setAppState});

  final RegisteredBackendsModel _registeredBackends = RegisteredBackendsModel();
  late Backend _activeBackend;
  final Function setAppState;
  bool isLoading = false;

  Future<void> initialize() async {
    isLoading = true;
    await _registeredBackends.initialize();
    _activeBackend = _registeredBackends.items[0];
    isLoading = false;

    setAppState(() {});
  }

  void connect(Backend backend) async {
    await backend.connect();
    setAppState(() {});
  }

  void disconnect(Backend backend) async {
    await backend.disconnect();
    setAppState(() {});
  }

  void activate(Backend backend) {
    _activeBackend = _registeredBackends.items
        .firstWhere((registeredBackend) => registeredBackend.id == backend.id);
    setAppState(() {});
  }

  void createNewBackend() {
    _registeredBackends.add(Backend.withDefaults());
    _activeBackend = _registeredBackends.items.last;
    setAppState(() {});
  }

  void updateBackend(Backend backend) {
    final id = _registeredBackends.items
        .firstWhere((registeredBackend) => registeredBackend.id == backend.id)
        .id;
    _registeredBackends.update(id, backend);
    _activeBackend = backend;
    setAppState(() {});
  }

  void removeActiveBackend() {
    _registeredBackends.remove(_activeBackend.id);
    if (_registeredBackends.items.isEmpty) {
      _registeredBackends.add(Backend.withDefaults());
    }
    _activeBackend = _registeredBackends.items[0];
    setAppState(() {});
  }

  UnmodifiableListView<Backend> get registeredBackends =>
      _registeredBackends.items;

  Backend get activeBackend => _activeBackend;

  bool isActive(Backend backend) => _activeBackend.id == backend.id;
}
