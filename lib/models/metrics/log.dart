class LogEntry {
  final String level;
  final String message;
  final String target;
  final dynamic span;
  final DateTime timestamp;
  final DateTime sourcedAt;

  LogEntry(
      {required this.level,
      required this.message,
      required this.target,
      this.span,
      required this.timestamp,
      required this.sourcedAt});

  factory LogEntry.fromJson(Map<String, dynamic> json) {
    return LogEntry(
      level: json['level'],
      message: json['message'],
      target: json['target'],
      span: json.containsKey('span') ? json['span'] : '',
      timestamp: DateTime.parse(json['timestamp']),
      sourcedAt: DateTime.parse(json['created_at']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'level': level,
      'message': message,
      'target': target,
      'span': span,
      'timestamp': timestamp.toIso8601String(),
      'created_at': sourcedAt.toIso8601String(),
    };
  }
}
