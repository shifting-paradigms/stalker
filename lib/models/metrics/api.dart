class ApiMetrics {
  final List<PrometheusMetric> httpRequestsTotal;
  final List<PrometheusMetric> httpRequestsDurationSeconds;

  ApiMetrics(
      {required this.httpRequestsTotal,
      required this.httpRequestsDurationSeconds});

  factory ApiMetrics.fromJson(Map<String, dynamic> json) {
    return ApiMetrics(
      httpRequestsTotal: (json['http_requests_total'] as List)
          .map((e) => PrometheusMetric.fromJson(e))
          .toList(),
      httpRequestsDurationSeconds:
          (json['http_requests_duration_seconds'] as List)
              .map((e) => PrometheusMetric.fromJson(e))
              .toList(),
    );
  }
}

class PrometheusMetric {
  final String name;
  final String method;
  final String path;
  final int status;
  final double? le;
  final double count;

  PrometheusMetric(
      {required this.name,
      required this.method,
      required this.path,
      required this.status,
      this.le,
      required this.count});

  factory PrometheusMetric.fromJson(Map<String, dynamic> json) {
    return PrometheusMetric(
      name: json['name'],
      method: json['method'],
      path: json['path'],
      status: json['status'],
      le: json['le'],
      count: json['count'],
    );
  }

  @override
  String toString() {
    return 'PrometheusMetric{ name: $name, method: $method, path: $path, status: $status, le: $le, count: $count }';
  }
}

typedef GroupedPrometheusMetrics
    = Map<String, Map<String, Map<String, List<PrometheusMetric>>>>;

GroupedPrometheusMetrics groupPrometheusMetrics(
    List<PrometheusMetric> prometheusMetrics) {
  final grouped = <String, Map<String, Map<String, List<PrometheusMetric>>>>{};
  for (var metric in prometheusMetrics) {
    final path = metric.path;
    final method = metric.method;
    final status = metric.status.toString();
    if (!grouped.containsKey(path)) {
      grouped[path] = {};
    }
    if (!grouped[path]!.containsKey(method)) {
      grouped[path]![method] = {};
    }
    if (!grouped[path]![method]!.containsKey(status)) {
      grouped[path]![method]![status] = [];
    }
    grouped[path]![method]![status]!.add(metric);
  }
  return grouped;
}

double getAverageDuration(List<PrometheusMetric> prometheusMetrics) {
  final sum = prometheusMetrics.singleWhere((element) {
    return element.name == 'http_requests_duration_seconds_sum';
  }).count;
  final count = prometheusMetrics.singleWhere((element) {
    return element.name == 'http_requests_duration_seconds_count';
  }).count;
  return sum / count;
}

List<KeyValuePair> getBucketCounts(List<PrometheusMetric> metrics) {
  final buckets = <KeyValuePair>[];
  var currentCount = 0;
  for (var metric in metrics) {
    if (metric.name != 'http_requests_duration_seconds_bucket') {
      continue;
    }
    if (metric.count > currentCount) {
      final diff = metric.count.toInt() - currentCount;
      final key = metric.le != null ? '${metric.le} s' : '20.0+ s';
      buckets.add(KeyValuePair(key, '$diff'));
      currentCount = metric.count.toInt();
    } else {
      final key = metric.le != null ? '${metric.le} s' : '20.0+ s';
      buckets.add(KeyValuePair(key, '-'));
    }
  }
  return buckets;
}

class KeyValuePair {
  const KeyValuePair(this.label, this.value);

  final String label;
  final String value;
}
