class HostMetrics {
  final DateTime timestamp;
  final String name;
  final String osVersion;
  final String kernel;
  final Duration uptime;
  final CpuMetrics cpu;
  final MemoryMetrics memory;
  final DiskMetrics disk;
  final IOMetrics io;

  HostMetrics(
      {required this.timestamp,
      required this.name,
      required this.osVersion,
      required this.kernel,
      required this.uptime,
      required this.cpu,
      required this.memory,
      required this.disk,
      required this.io});

  factory HostMetrics.fromJson(Map<String, dynamic> json) {
    return HostMetrics(
      timestamp: DateTime.fromMillisecondsSinceEpoch(json['timestamp'] * 1000),
      name: json['name'],
      osVersion: json['os_version'],
      kernel: json['kernel'],
      uptime: Duration(seconds: json['uptime']),
      cpu: CpuMetrics.fromJson(json['cpu']),
      memory: MemoryMetrics.fromJson(json['memory']),
      disk: DiskMetrics.fromJson(json['disk']),
      io: IOMetrics.fromJson(json['io']),
    );
  }
}

class CpuMetrics {
  final String brand;
  final int cores;
  final double usage;
  final double avgLoadOne;
  final double avgLoadFive;
  final double avgLoadFifteen;

  CpuMetrics(
      {required this.brand,
      required this.cores,
      required this.usage,
      required this.avgLoadOne,
      required this.avgLoadFive,
      required this.avgLoadFifteen});

  factory CpuMetrics.fromJson(Map<String, dynamic> json) {
    return CpuMetrics(
      brand: json['brand'],
      cores: json['cores'],
      usage: json['usage'],
      avgLoadOne: json['avg_load_one'],
      avgLoadFive: json['avg_load_five'],
      avgLoadFifteen: json['avg_load_fifteen'],
    );
  }
}

class MemoryMetrics {
  final int total;
  final int free;
  final int available;
  final int used;

  MemoryMetrics(
      {required this.total,
      required this.free,
      required this.available,
      required this.used});

  factory MemoryMetrics.fromJson(Map<String, dynamic> json) {
    return MemoryMetrics(
      total: json['total'],
      free: json['free'],
      available: json['available'],
      used: json['used'],
    );
  }

  double get percentage => used / total;
}

class DiskMetrics {
  final int total;
  final int used;

  DiskMetrics({required this.total, required this.used});

  factory DiskMetrics.fromJson(Map<String, dynamic> json) {
    return DiskMetrics(
      total: json['total'],
      used: json['used'],
    );
  }

  double get percentage => used / total;
}

class IOMetrics {
  final int received;
  final int transmitted;

  IOMetrics({required this.received, required this.transmitted});

  factory IOMetrics.fromJson(Map<String, dynamic> json) {
    return IOMetrics(
      received: json['received'],
      transmitted: json['transmitted'],
    );
  }
}
