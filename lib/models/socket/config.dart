import 'dart:collection';
import 'dart:convert';

class StreamConfig {
  final HostConfig host;
  final ApiConfig api;
  final LogConfig log;

  StreamConfig({required this.host, required this.api, required this.log});

  factory StreamConfig.withDefaults() {
    return StreamConfig(
        host: HostConfig.withDefaults(),
        api: ApiConfig.withDefaults(),
        log: LogConfig.withDefaults());
  }

  factory StreamConfig.fromJson(Map<String, dynamic> json) {
    return StreamConfig(
        host: HostConfig.fromJson(json['host']),
        api: ApiConfig.fromJson(json['api']),
        log: LogConfig.fromJson(json['log']));
  }

  toJson() {
    return {
      'host': host.toJson(),
      'api': api.toJson(),
      'log': log.toJson(),
    };
  }

  asPackage() {
    return jsonEncode(toJson());
  }
}

class HostConfig {
  int interval;

  HostConfig({required this.interval});

  factory HostConfig.withDefaults() {
    return HostConfig(interval: 1000);
  }

  factory HostConfig.fromJson(Map<String, dynamic> json) {
    return HostConfig(interval: json['interval']);
  }

  toJson() {
    return {
      'interval': interval,
    };
  }
}

class ApiConfig {
  int interval;

  ApiConfig({required this.interval});

  factory ApiConfig.withDefaults() {
    return ApiConfig(interval: 1000);
  }

  factory ApiConfig.fromJson(Map<String, dynamic> json) {
    return ApiConfig(interval: json['interval']);
  }

  toJson() {
    return {
      'interval': interval,
    };
  }
}

class LogConfig {
  int interval;
  DateTime startTimestamp;

  LogConfig({required this.interval, required this.startTimestamp});

  factory LogConfig.withDefaults() {
    return LogConfig(interval: 1000, startTimestamp: DateTime.now().toUtc());
  }

  factory LogConfig.fromJson(Map<String, dynamic> json) {
    return LogConfig(
        interval: json['interval'],
        startTimestamp: DateTime.parse(json['start_timestamp']));
  }

  toJson() {
    return {
      'interval': interval,
      'start_timestamp': startTimestamp.toUtc().toIso8601String(),
    };
  }
}

class StreamConfigsModel {
  final Map<String, StreamConfig> _items = {};

  UnmodifiableMapView<String, StreamConfig> get items =>
      UnmodifiableMapView(_items);

  void add(String backendId, StreamConfig config) {
    _items[backendId] = config;
  }

  void remove(String backendId) {
    _items.remove(backendId);
  }

  void update(String backendId, StreamConfig config) {
    _items[backendId] = config;
  }

  StreamConfig? get(String backendId) {
    return _items[backendId];
  }
}
