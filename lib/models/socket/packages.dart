import 'package:insights_client/models/metrics/host.dart';
import 'package:insights_client/models/metrics/api.dart';
import 'package:insights_client/models/metrics/log.dart';

enum PackageKind {
  hostMetrics,
  apiMetrics,
  logEntry,
}

extension PackageKindExtension on PackageKind {
  String asString() {
    switch (this) {
      case PackageKind.hostMetrics:
        return 'HostMetrics';
      case PackageKind.apiMetrics:
        return 'ApiMetrics';
      case PackageKind.logEntry:
        return 'LogEntry';
    }
  }

  static PackageKind fromString(String value) {
    switch (value) {
      case 'HostMetrics':
        return PackageKind.hostMetrics;
      case 'ApiMetrics':
        return PackageKind.apiMetrics;
      case 'LogEntry':
        return PackageKind.logEntry;
      default:
        throw Exception('Unknown PackageKind: $value');
    }
  }
}

class MetricsPackage {
  final String id;
  final PackageKind kind;
  final dynamic data;

  MetricsPackage(this.id, this.kind, this.data);

  factory MetricsPackage.fromJson(Map<String, dynamic> json) {
    return MetricsPackage(
      json['id'],
      PackageKindExtension.fromString(json['kind']),
      json['data'],
    );
  }

  parse() {
    switch (kind) {
      case PackageKind.hostMetrics:
        return HostMetrics.fromJson(data);
      case PackageKind.apiMetrics:
        return ApiMetrics.fromJson(data);
      case PackageKind.logEntry:
        return LogEntry.fromJson(data);
    }
  }
}
