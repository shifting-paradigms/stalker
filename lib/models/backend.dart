import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:insights_client/models/socket/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:web_socket_channel/io.dart';

class Backend {
  String id;
  String name;
  String address;
  String apiKey;
  StreamConfig streamConfig;
  BackendState state = BackendState.disconnected;
  BackendSocket? socket;

  Backend({
    required this.id,
    required this.name,
    required this.address,
    required this.apiKey,
    required this.streamConfig,
  });

  factory Backend.withDefaults() {
    return Backend(
        id: const Uuid().v4(),
        name: 'New Backend',
        address: '',
        apiKey: '',
        streamConfig: StreamConfig.withDefaults());
  }

  factory Backend.fromJson(Map<String, dynamic> json) {
    return Backend(
        id: json['id'],
        name: json['name'],
        address: json['address'],
        apiKey: json['apiKey'],
        streamConfig: StreamConfig.fromJson(json['streamConfig']));
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'address': address,
      'apiKey': apiKey,
      'streamConfig': streamConfig.toJson()
    };
  }

  void setState(BackendState state) {
    this.state = state;
  }

  Future<void> connect() async {
    if (socket == null) {
      final wsUrl = Uri.parse(address);
      final header = {'Authorization': 'Bearer $apiKey'};
      try {
        final channel = IOWebSocketChannel.connect(wsUrl, headers: header);
        await channel.ready; // <- this is needed to catch errors
        socket = BackendSocket.fromChannel(channel);
        socket?.sink.add(jsonEncode(streamConfig.toJson()));
        state = BackendState.connected;
      } on SocketException catch (_) {
        state = BackendState.offline;
      } on WebSocketException catch (_) {
        state = BackendState.invalid;
      }
    }
  }

  Future<void> disconnect() async {
    socket?.sink.close();
    socket = null;
    state = BackendState.disconnected;
  }

  bool isConnected() {
    return state == BackendState.connected;
  }

  getStream() {
    return socket?.stream;
  }

  void setHostInterval(int interval) {
    streamConfig.host.interval = interval;
    socket?.sink.add(jsonEncode(streamConfig.toJson()));
  }

  void setApiInterval(int interval) {
    streamConfig.api.interval = interval;
    socket?.sink.add(jsonEncode(streamConfig.toJson()));
  }

  void setLogInterval(int interval) {
    streamConfig.log.interval = interval;
    socket?.sink.add(jsonEncode(streamConfig.toJson()));
  }

  void setLogStartTimestamp(DateTime timestamp) {
    streamConfig.log.startTimestamp = timestamp;
    socket?.sink.add(jsonEncode(streamConfig.toJson()));
  }
}

enum BackendState { connected, disconnected, offline, invalid }

extension BackendStateExtension on BackendState {
  static BackendState fromConnectionState(ConnectionState state) {
    switch (state) {
      case ConnectionState.waiting:
        return BackendState.connected;
      case ConnectionState.active:
        return BackendState.connected;
      case ConnectionState.done:
        return BackendState.disconnected;
      case ConnectionState.none:
        return BackendState.disconnected;
      default:
        return BackendState.disconnected;
    }
  }
}

class BackendSocket {
  final IOWebSocketChannel channel;
  final Stream stream;
  final Sink sink;

  BackendSocket({
    required this.channel,
    required this.stream,
    required this.sink,
  });

  factory BackendSocket.fromChannel(IOWebSocketChannel channel) {
    return BackendSocket(
        channel: channel,
        stream: channel.stream.asBroadcastStream(),
        sink: channel.sink);
  }
}

class RegisteredBackendsModel {
  final List<Backend> _items = [];

  UnmodifiableListView<Backend> get items => UnmodifiableListView(_items);

  Future<void> initialize() async {
    var backends = await _load();
    if (backends.isNotEmpty) {
      _items.addAll(backends);
    } else {
      _items.add(Backend.withDefaults());
    }
  }

  void add(Backend backend) {
    _items.add(backend);
    _save();
  }

  void addAll(List<Backend> backends) {
    _items.addAll(backends);
    _save();
  }

  void remove(String id) {
    _items.removeWhere((backend) => backend.id == id);
    _save();
  }

  void update(String id, Backend backend) {
    var index = _items.indexWhere((backend) => backend.id == id);
    _items[index] = backend;
    _save();
  }

  Future<List<Backend>> _load() async {
    final prefs = await SharedPreferences.getInstance();
    final jsonStrings = prefs.getStringList('backends');
    if (jsonStrings == null) {
      return [];
    }
    final backends = jsonStrings
        .map(
          (backend) => Backend.fromJson(jsonDecode(backend)),
        )
        .toList();
    for (var i = 0; i < backends.length; i++) {
      // This overrides the startTimestamp with the current time to allow
      // the user to start a new log stream on demand.
      backends[i].streamConfig.log.startTimestamp = DateTime.now().toUtc();
    }

    return backends;
  }

  Future<void> _save() async {
    final prefs = await SharedPreferences.getInstance();
    final jsonStrings =
        _items.map((backend) => jsonEncode(backend.toJson())).toList();
    prefs.setStringList('backends', jsonStrings);
  }
}
