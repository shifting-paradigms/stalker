import 'package:flutter/material.dart';
import 'package:insights_client/views/dashboards/utils.dart';
import 'package:insights_client/views/dashboards/log/filter/filter.dart';

class LogLevelFilterBar extends StatefulWidget {
  const LogLevelFilterBar(
      {super.key,
      required this.filterController,
      required this.setParentState});

  final FilterController filterController;
  final Function setParentState;

  @override
  LogLevelFilterBarState createState() => LogLevelFilterBarState();
}

class LogLevelFilterBarState extends State<LogLevelFilterBar> {
  @override
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(widget
                    .filterController.levelFilters
                    .contains(LevelFilterValue.error)
                ? logLevelToColor("ERROR")
                : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.error();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "ERROR",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(widget
                    .filterController.levelFilters
                    .contains(LevelFilterValue.warn)
                ? logLevelToColor("WARN")
                : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.warn();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "WARN",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(widget
                    .filterController.levelFilters
                    .contains(LevelFilterValue.info)
                ? logLevelToColor("INFO")
                : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.info();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "INFO",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(widget
                    .filterController.levelFilters
                    .contains(LevelFilterValue.debug)
                ? logLevelToColor("DEBUG")
                : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.debug();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "DEBUG",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(widget
                    .filterController.levelFilters
                    .contains(LevelFilterValue.trace)
                ? logLevelToColor("TRACE")
                : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.trace();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "TRACE",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }
}

enum LevelFilterValue {
  trace,
  debug,
  info,
  warn,
  error,
}

extension LevelFilterValueExtension on LevelFilterValue {
  String asString() {
    switch (this) {
      case LevelFilterValue.trace:
        return "TRACE";
      case LevelFilterValue.debug:
        return "DEBUG";
      case LevelFilterValue.info:
        return "INFO";
      case LevelFilterValue.warn:
        return "WARN";
      case LevelFilterValue.error:
        return "ERROR";
    }
  }

  static LevelFilterValue fromString(String level) {
    switch (level) {
      case "TRACE":
        return LevelFilterValue.trace;
      case "DEBUG":
        return LevelFilterValue.debug;
      case "INFO":
        return LevelFilterValue.info;
      case "WARN":
        return LevelFilterValue.warn;
      case "ERROR":
        return LevelFilterValue.error;
      default:
        throw Exception("Invalid level filter value");
    }
  }
}
