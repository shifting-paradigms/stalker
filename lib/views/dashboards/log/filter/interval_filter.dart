import 'package:flutter/material.dart';
import 'package:insights_client/views/dashboards/log/filter/filter.dart';

class IntervalFilterBar extends StatefulWidget {
  const IntervalFilterBar(
      {super.key,
      required this.filterController,
      required this.setParentState});

  final FilterController filterController;
  final Function setParentState;

  @override
  IntervalFilterBarState createState() => IntervalFilterBarState();
}

class IntervalFilterBarState extends State<IntervalFilterBar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                widget.filterController.intervalFilter ==
                        IntervalFilterValue.lastHour
                    ? Colors.blue
                    : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.lastHour();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "Last Hour",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                widget.filterController.intervalFilter ==
                        IntervalFilterValue.lastDay
                    ? Colors.blue
                    : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.lastDay();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "Last Day",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                widget.filterController.intervalFilter ==
                        IntervalFilterValue.lastWeek
                    ? Colors.blue
                    : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.lastWeek();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "Last Week",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                widget.filterController.intervalFilter ==
                        IntervalFilterValue.lastMonth
                    ? Colors.blue
                    : Colors.grey.shade700),
          ),
          onPressed: () {
            widget.filterController.lastMonth();
            setState(() {});
            widget.setParentState(() {});
          },
          child: const Text(
            "Last Month",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }
}

enum IntervalFilterValue {
  lastHour,
  lastDay,
  lastWeek,
  lastMonth,
  allTime,
}

extension IntervalFilterValueExtension on IntervalFilterValue {
  DateTime get timestamp {
    final now = DateTime.now();
    switch (this) {
      case IntervalFilterValue.lastHour:
        return now.subtract(const Duration(hours: 1));
      case IntervalFilterValue.lastDay:
        return now.subtract(const Duration(days: 1));
      case IntervalFilterValue.lastWeek:
        return now.subtract(const Duration(days: 7));
      case IntervalFilterValue.lastMonth:
        return now.subtract(const Duration(days: 30));
      case IntervalFilterValue.allTime:
        return DateTime(0);
    }
  }
}
