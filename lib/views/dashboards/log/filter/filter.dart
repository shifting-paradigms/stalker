import 'package:flutter/material.dart';
import 'package:insights_client/models/metrics/log.dart';
import 'package:insights_client/views/dashboards/log/filter/level_filter.dart';
import 'package:insights_client/views/dashboards/log/filter/interval_filter.dart';

class LogListFilter extends StatelessWidget {
  const LogListFilter(
      {super.key,
      required this.filterController,
      required this.setParentState});

  final FilterController filterController;
  final Function setParentState;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            LogLevelFilterBar(
              filterController: filterController,
              setParentState: setParentState,
            ),
            const SizedBox(
              height: 10,
            ),
            IntervalFilterBar(
              filterController: filterController,
              setParentState: setParentState,
            ),
            const SizedBox(
              height: 8,
            ),
          ],
        )
      ],
    );
  }
}

class FilterController {
  List<LevelFilterValue> levelFilters = [];
  IntervalFilterValue intervalFilter = IntervalFilterValue.allTime;

  List<LogEntry> filter(List<LogEntry> entries) {
    List<LogEntry> filteredEntries = entries;

    // if levelfilters is empty and intervalFilter is allTime, return all entries
    if (levelFilters.isEmpty && intervalFilter == IntervalFilterValue.allTime) {
      return entries;
    }

    // if levelFilters is empty and intervalFilter is not allTime, filter by interval
    if (levelFilters.isEmpty && intervalFilter != IntervalFilterValue.allTime) {
      filteredEntries = entries
          .where((entry) => entry.timestamp.isAfter(intervalFilter.timestamp))
          .toList();
      return filteredEntries;
    }

    // if levelFilters is not empty and intervalFilter is allTime, filter by level
    if (levelFilters.isNotEmpty &&
        intervalFilter == IntervalFilterValue.allTime) {
      filteredEntries = entries
          .where((entry) => levelFilters
              .contains(LevelFilterValueExtension.fromString(entry.level)))
          .toList();
      return filteredEntries;
    }

    // if levelFilters is not empty and intervalFilter is not allTime,
    // filter by level and interval
    if (levelFilters.isNotEmpty &&
        intervalFilter != IntervalFilterValue.allTime) {
      filteredEntries = entries
          .where((entry) =>
              levelFilters.contains(
                  LevelFilterValueExtension.fromString(entry.level)) &&
              entry.timestamp.isAfter(intervalFilter.timestamp))
          .toList();
      return filteredEntries;
    }

    return entries;
  }

  void _setLevelFilter(LevelFilterValue value) {
    levelFilters.add(value);
  }

  void _unsetLevelFilter(LevelFilterValue value) {
    levelFilters.remove(value);
  }

  void _setIntervalFilter(IntervalFilterValue value) {
    intervalFilter = value;
  }

  void clearLevelFilters() {
    levelFilters = [];
  }

  void clearIntervalFilter() {
    intervalFilter = IntervalFilterValue.allTime;
  }

  void clear() {
    clearLevelFilters();
    clearIntervalFilter();
  }

  void trace() {
    if (levelFilters.contains(LevelFilterValue.trace)) {
      _unsetLevelFilter(LevelFilterValue.trace);
    } else {
      _setLevelFilter(LevelFilterValue.trace);
    }
  }

  void debug() {
    if (levelFilters.contains(LevelFilterValue.debug)) {
      _unsetLevelFilter(LevelFilterValue.debug);
    } else {
      _setLevelFilter(LevelFilterValue.debug);
    }
  }

  void info() {
    if (levelFilters.contains(LevelFilterValue.info)) {
      _unsetLevelFilter(LevelFilterValue.info);
    } else {
      _setLevelFilter(LevelFilterValue.info);
    }
  }

  void warn() {
    if (levelFilters.contains(LevelFilterValue.warn)) {
      _unsetLevelFilter(LevelFilterValue.warn);
    } else {
      _setLevelFilter(LevelFilterValue.warn);
    }
  }

  void error() {
    if (levelFilters.contains(LevelFilterValue.error)) {
      _unsetLevelFilter(LevelFilterValue.error);
    } else {
      _setLevelFilter(LevelFilterValue.error);
    }
  }

  void lastHour() {
    if (intervalFilter == IntervalFilterValue.lastHour) {
      clearIntervalFilter();
    } else {
      _setIntervalFilter(IntervalFilterValue.lastHour);
    }
  }

  void lastDay() {
    if (intervalFilter == IntervalFilterValue.lastDay) {
      clearIntervalFilter();
    } else {
      _setIntervalFilter(IntervalFilterValue.lastDay);
    }
  }

  void lastWeek() {
    if (intervalFilter == IntervalFilterValue.lastWeek) {
      clearIntervalFilter();
    } else {
      _setIntervalFilter(IntervalFilterValue.lastWeek);
    }
  }

  void lastMonth() {
    if (intervalFilter == IntervalFilterValue.lastMonth) {
      clearIntervalFilter();
    } else {
      _setIntervalFilter(IntervalFilterValue.lastMonth);
    }
  }

  List<LevelFilterValue> getLevelFilters() {
    return levelFilters;
  }

  IntervalFilterValue? getIntervalFilter() {
    return intervalFilter;
  }
}
