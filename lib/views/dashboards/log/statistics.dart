import 'package:flutter/material.dart';
import 'package:insights_client/models/metrics/log.dart';
import 'package:insights_client/views/dashboards/api/requests_duration.dart';
import 'package:insights_client/views/dashboards/utils.dart';

class LogStats {
  final List<LogEntry> logEntries;
  int totalEntries = 0;
  int totalErrors = 0;
  int totalWarnings = 0;
  int totalInfos = 0;
  int totalDebugs = 0;
  int totalTraces = 0;
  int totalUnknowns = 0;

  // calculate counts in constructor
  LogStats({required this.logEntries}) {
    _update(logEntries);
  }

  void _update(List<LogEntry> logEntries) {
    totalEntries = logEntries.length;
    for (var entry in logEntries) {
      switch (entry.level) {
        case 'ERROR':
          totalErrors++;
          break;
        case 'WARN':
          totalWarnings++;
          break;
        case 'INFO':
          totalInfos++;
          break;
        case 'DEBUG':
          totalDebugs++;
          break;
        case 'TRACE':
          totalTraces++;
          break;
        default:
          totalUnknowns++;
          break;
      }
    }
  }
}

class LogStatsBarView extends StatelessWidget {
  const LogStatsBarView({
    super.key,
    required this.logStats,
  });

  final LogStats logStats;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SizedBox(
                width: 100,
                height: 100,
                child: LabeledValueView(
                  label: 'total',
                  value: logStats.totalEntries.toString(),
                ),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: LogEventCountView(
                    count: logStats.totalErrors, label: 'ERROR'),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: LogEventCountView(
                    count: logStats.totalWarnings, label: 'WARN'),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: LogEventCountView(
                    count: logStats.totalInfos, label: 'INFO'),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: LogEventCountView(
                    count: logStats.totalDebugs, label: 'DEBUG'),
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: LogEventCountView(
                    count: logStats.totalTraces, label: 'TRACE'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LogEventCountView extends StatelessWidget {
  const LogEventCountView({
    super.key,
    required this.count,
    required this.label,
  });

  final int count;
  final String label;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  count.toString(),
                  style: const TextStyle(
                      fontSize: 32, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                color: logLevelToColor(label),
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(4),
                  bottomRight: Radius.circular(4),
                )),
            padding: const EdgeInsets.all(4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  label,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
