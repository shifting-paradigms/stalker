import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:insights_client/controller/backend.dart';
import 'package:insights_client/models/backend.dart';
import 'package:insights_client/models/metrics/log.dart';
import 'package:insights_client/models/socket/packages.dart';
import 'package:insights_client/views/dashboards/log/filter/filter.dart';
import 'package:insights_client/views/dashboards/log/statistics.dart';
import 'package:insights_client/views/dashboards/log/list.dart';
import 'package:insights_client/views/socket.dart';

class LogDashboardWrapper extends StatefulWidget {
  const LogDashboardWrapper(
      {super.key, required this.snapshot, required this.backendController});

  final AsyncSnapshot snapshot;
  final BackendController backendController;

  @override
  State<LogDashboardWrapper> createState() => _LogDashboardWrapperState();
}

class _LogDashboardWrapperState extends State<LogDashboardWrapper>
    with AutomaticKeepAliveClientMixin {
  List<LogEntry> logEntries = [];
  late DateTime _timestampOfLastLogEntry;
  DateTime _timer = DateTime.now();
  final Duration _timerThreshold = const Duration(seconds: 1);
  final Duration _timerUpperLimit = const Duration(seconds: 3);
  bool _isFetching = false;
  Timer _fetchFuture = Timer(Duration.zero, () {});

  Duration _timerDurationLeft() {
    if (_timer.isAfter(DateTime.now())) {
      return _timer.difference(DateTime.now());
    } else {
      return const Duration();
    }
  }

  void _resetTimer({durationAddition = const Duration(milliseconds: 100)}) {
    _timer = DateTime.now().add(_timerDurationLeft() + durationAddition);
    if (_timerDurationLeft() > _timerUpperLimit) {
      _timer = DateTime.now().add(_timerUpperLimit);
    }
    if (_timerThresholdReached()) {
      _isFetching = true;
      _fetchFuture.cancel();
      _fetchFuture = Timer(const Duration(seconds: 3), () {
        _isFetching = false;
        widget.backendController.activeBackend
            .setLogStartTimestamp(_timestampOfLastLogEntry);
        setState(() {});
      });
    }
  }

  bool _timerThresholdReached() {
    return _timerDurationLeft() > _timerThreshold;
  }

  void _startDownload() {
    logEntries.clear();
    _resetTimer(durationAddition: _timerUpperLimit);
    final epoch = DateTime(1970);
    widget.backendController.activeBackend.setLogStartTimestamp(epoch);
    setState(() {});
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    final backendState = BackendStateExtension.fromConnectionState(
        widget.snapshot.connectionState);
    if (backendState == BackendState.disconnected) {
      widget.backendController
          .disconnect(widget.backendController.activeBackend);
    }
    _timestampOfLastLogEntry =
        widget.backendController.activeBackend.streamConfig.log.startTimestamp;
  }

  @override
  void didUpdateWidget(covariant LogDashboardWrapper oldWidget) {
    super.didUpdateWidget(oldWidget);
    final backendState = BackendStateExtension.fromConnectionState(
        widget.snapshot.connectionState);
    if (backendState == BackendState.disconnected) {
      widget.backendController
          .disconnect(widget.backendController.activeBackend);
    }
    if (widget.snapshot.hasData) {
      final package = MetricsPackage.fromJson(
        jsonDecode(widget.snapshot.data),
      );

      if (package.kind == PackageKind.logEntry) {
        final logEntry = LogEntry.fromJson(package.data);
        if (logEntries
            .where((element) =>
                element.toJson().toString() == logEntry.toJson().toString())
            .isEmpty) {
          // This is not efficient, but it will do for now.
          // Scales poorly with amount of log entries.
          // TODO: imrove performance and algorithm complexity.

          _resetTimer();
          logEntries.add(LogEntry.fromJson(package.data));

          if (_isFetching) {
            _timestampOfLastLogEntry = logEntry.timestamp;
          } else {
            widget.backendController.activeBackend
                .setLogStartTimestamp(_timestampOfLastLogEntry);
          }
          setState(() {});
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (_isFetching) {
      return Center(
        child: Text(
          'Fetching Log Entries: ${logEntries.length}',
          style: const TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    } else {
      return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height - 104,
        child: Stack(
          children: [
            logEntries.isNotEmpty
                ? Column(
                    children: [
                      FilterableLogListView(
                          logEntries: logEntries, updateLogDashboard: setState),
                    ],
                  )
                : const NoData(),
            Positioned(
              bottom: 20,
              right: 20,
              child: ElevatedButton.icon(
                label: const Text('Fetch All'),
                icon: const Icon(Icons.refresh),
                onPressed: () {
                  _startDownload();
                },
              ),
            ),
          ],
        ),
      );
    }
  }
}

class FilterableLogListView extends StatefulWidget {
  const FilterableLogListView(
      {super.key, required this.logEntries, required this.updateLogDashboard});

  final List<LogEntry> logEntries;
  final Function updateLogDashboard;

  @override
  State<FilterableLogListView> createState() => _FilterableLogListViewState();
}

class _FilterableLogListViewState extends State<FilterableLogListView> {
  LogStats logStats = LogStats(logEntries: []);
  List<LogEntry> filteredLogEntries = [];
  FilterController filterController = FilterController();

  @override
  void initState() {
    super.initState();
    filteredLogEntries = filterController.filter(widget.logEntries);
    logStats = LogStats(logEntries: filteredLogEntries);
  }

  @override
  void didUpdateWidget(covariant FilterableLogListView oldWidget) {
    super.didUpdateWidget(oldWidget);
    filteredLogEntries = filterController.filter(widget.logEntries);
    logStats = LogStats(logEntries: filteredLogEntries);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24, left: 8, right: 8),
      child: Column(
        children: [
          LogStatsBarView(logStats: logStats),
          const SizedBox(height: 8),
          const Divider(),
          const SizedBox(height: 8),
          LogListFilter(
              filterController: filterController,
              setParentState: widget.updateLogDashboard),
          const SizedBox(height: 8),
          SizedBox(
            height: MediaQuery.of(context).size.height - 312 - 40,
            child: LogListView(
              filteredLogEntries: filteredLogEntries,
            ),
          ),
        ],
      ),
    );
  }
}
