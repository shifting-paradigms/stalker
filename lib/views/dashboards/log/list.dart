import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:insights_client/models/metrics/log.dart';

class LogListView extends StatefulWidget {
  const LogListView({
    super.key,
    required this.filteredLogEntries,
  });

  final List<LogEntry> filteredLogEntries;

  @override
  State<LogListView> createState() => _LogListViewState();
}

class _LogListViewState extends State<LogListView> {
  final ScrollController scrollController = ScrollController();
  bool _isScrolling = false;

  void _updateScrollState() {
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      _isScrolling = false;
    } else if (scrollController.position.userScrollDirection ==
        ScrollDirection.reverse) {
      _isScrolling = true;
    } else if (scrollController.position.userScrollDirection ==
        ScrollDirection.forward) {
      _isScrolling = true;
    } else {
      _isScrolling = false;
    }
    setState(() {});
  }

  void _scrollDown({bool force = false}) {
    if (force || !_isScrolling) {
      scrollController.animateTo(
        scrollController.position.maxScrollExtent,
        duration: const Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_updateScrollState);

    Future.delayed(const Duration(milliseconds: 100), () {
      _scrollDown();
    });
  }

  @override
  void didUpdateWidget(covariant LogListView oldWidget) {
    super.didUpdateWidget(oldWidget);

    Future.delayed(const Duration(milliseconds: 100), () {
      _scrollDown();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Expanded(
              child: ListView.builder(
                addSemanticIndexes: true,
                reverse: true,
                controller: scrollController,
                physics: const BouncingScrollPhysics(),
                itemCount: widget.filteredLogEntries.length,
                itemBuilder: (context, index) {
                  return LogEntryView(
                    logEntry: widget.filteredLogEntries[index],
                  );
                },
              ),
            ),
          ],
        ),
        _isScrolling
            ? Positioned(
                top: 20,
                right: 20,
                child: FloatingActionButton(
                  backgroundColor: Colors.lightBlue,
                  onPressed: () {
                    _scrollDown(force: true);
                  },
                  child: const Icon(Icons.arrow_upward, color: Colors.white),
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}

Color logLevelToColor(String level) {
  switch (level) {
    case 'ERROR':
      return Colors.red;
    case 'WARN':
      return Colors.orange;
    case 'INFO':
      return Colors.green;
    case 'DEBUG':
      return Colors.blue;
    case 'TRACE':
      return Colors.purple;
    default:
      return Colors.black;
  }
}

class LogEntryView extends StatefulWidget {
  const LogEntryView({super.key, required this.logEntry});

  final LogEntry logEntry;

  @override
  State<LogEntryView> createState() => _LogEntryViewState();
}

class _LogEntryViewState extends State<LogEntryView> {
  bool _isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Card(
        child: IntrinsicHeight(
          child: Row(
            children: [
              Container(
                width: 25,
                decoration: BoxDecoration(
                    color: logLevelToColor(widget.logEntry.level),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(4.0),
                      bottomLeft: Radius.circular(4.0),
                    )),
                padding: const EdgeInsets.all(4.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    for (var letter in widget.logEntry.level.split(''))
                      Text(
                        letter,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: MediaQuery.of(context).size.width - 65,
                  constraints: const BoxConstraints(
                    maxWidth: 800,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        height: _isExpanded ? null : 40,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Flexible(
                                  child: Column(
                                    children: [
                                      Text(
                                        widget.logEntry.message,
                                        maxLines: _isExpanded ? null : 2,
                                        overflow: _isExpanded
                                            ? null
                                            : TextOverflow.ellipsis,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _isExpanded = !_isExpanded;
                                    });
                                  },
                                  icon: Icon(_isExpanded
                                      ? Icons.arrow_drop_up
                                      : Icons.arrow_drop_down),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'target: ${widget.logEntry.target}',
                                  ),
                                ],
                              ),
                              if (widget.logEntry.span != null)
                                Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: widget.logEntry.span
                                          .toString()
                                          .replaceAll(RegExp(r'[{}]'), '')
                                          .split(',')
                                          .map((e) => Text(e.trim()))
                                          .toList(),
                                    ),
                                  ],
                                )
                            ],
                          ),
                          const SizedBox(width: 16.0),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                widget.logEntry.timestamp
                                    .toIso8601String()
                                    .split('.')[0]
                                    .replaceAll('T', ' '),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
