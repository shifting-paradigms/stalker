import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:insights_client/controller/backend.dart';
import 'package:insights_client/models/backend.dart';
import 'package:insights_client/models/metrics/api.dart';
import 'package:insights_client/models/socket/packages.dart';
import 'package:insights_client/views/socket.dart';
import 'package:insights_client/views/dashboards/api/requests_total.dart';
import 'package:insights_client/views/dashboards/api/requests_duration.dart';

class ApiDashboardWrapper extends StatefulWidget {
  const ApiDashboardWrapper(
      {super.key, required this.snapshot, required this.backendController});

  final AsyncSnapshot snapshot;
  final BackendController backendController;

  @override
  State<ApiDashboardWrapper> createState() => _ApiDashboardWrapperState();
}

class _ApiDashboardWrapperState extends State<ApiDashboardWrapper>
    with AutomaticKeepAliveClientMixin {
  ApiMetrics? apiMetrics;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    final backendState = BackendStateExtension.fromConnectionState(
        widget.snapshot.connectionState);
    if (backendState == BackendState.disconnected) {
      widget.backendController
          .disconnect(widget.backendController.activeBackend);
    }
  }

  @override
  void didUpdateWidget(covariant ApiDashboardWrapper oldWidget) {
    super.didUpdateWidget(oldWidget);
    final backendState = BackendStateExtension.fromConnectionState(
        widget.snapshot.connectionState);
    if (backendState == BackendState.disconnected) {
      widget.backendController
          .disconnect(widget.backendController.activeBackend);
    }
    if (widget.snapshot.hasData) {
      final package = MetricsPackage.fromJson(
        jsonDecode(widget.snapshot.data),
      );
      if (package.kind == PackageKind.apiMetrics) {
        apiMetrics = ApiMetrics.fromJson(package.data);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return (apiMetrics != null)
        ? ApiDashboard(apiMetrics: apiMetrics!)
        : const NoData();
  }
}

class ApiDashboard extends StatelessWidget {
  const ApiDashboard({super.key, required this.apiMetrics});

  final ApiMetrics apiMetrics;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: 16.0, right: 16.0, top: 24.0, bottom: 24.0),
      child: ListView(
        shrinkWrap: true,
        children: [
          const Text(
            'Prometheus Metrics',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          const Divider(),
          RequestsTotalView(httpRequestsTotal: apiMetrics.httpRequestsTotal),
          const Divider(),
          RequestsDurationView(
              httpRequestsDurationSeconds:
                  apiMetrics.httpRequestsDurationSeconds),
        ],
      ),
    );
  }
}
