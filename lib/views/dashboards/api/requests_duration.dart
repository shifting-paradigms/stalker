import 'package:flutter/material.dart';

import 'package:insights_client/models/metrics/api.dart';
import 'package:insights_client/views/dashboards/utils.dart';

class RequestsDurationView extends StatelessWidget {
  const RequestsDurationView({
    super.key,
    required this.httpRequestsDurationSeconds,
  });

  final List<PrometheusMetric> httpRequestsDurationSeconds;

  @override
  Widget build(BuildContext context) {
    final groupedMetrics = groupPrometheusMetrics(httpRequestsDurationSeconds);

    return Column(
      children: <Widget>[
        Row(
          children: const [
            Text('Requests Duration', style: TextStyle(fontSize: 24)),
          ],
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              for (var path in groupedMetrics.keys)
                for (var method in groupedMetrics[path]!.keys)
                  for (var status in groupedMetrics[path]![method]!.keys)
                    RequestsDurationViewItem(
                      prometheusMetrics:
                          groupedMetrics[path]![method]![status]!,
                      path: path,
                      method: method,
                      status: status,
                    ),
            ],
          ),
        ),
      ],
    );
  }
}

class RequestsDurationViewItem extends StatelessWidget {
  const RequestsDurationViewItem(
      {super.key,
      required this.prometheusMetrics,
      required this.path,
      required this.method,
      required this.status});

  final List<PrometheusMetric> prometheusMetrics;
  final String path;
  final String method;
  final String status;

  String _toReadableDuration(double duration) {
    if (duration < 0.001) {
      return '${(duration * 1000000).toStringAsFixed(2)} µs';
    } else if (duration < 1) {
      return '${(duration * 1000).toStringAsFixed(2)} ms';
    } else {
      return '${duration.toStringAsFixed(2)} s';
    }
  }

  @override
  Widget build(BuildContext context) {
    final bucketCounts = getBucketCounts(prometheusMetrics);
    final avgDuration = getAverageDuration(prometheusMetrics);
    final valueString = _toReadableDuration(avgDuration);

    return SizedBox(
      height: 100,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 150,
              child: RequestsDurationViewItemLabel(
                  path: path, method: method, status: status),
            ),
            const VerticalDivider(),
            SizedBox(
                width: 150,
                child: LabeledValueView(label: 'Average', value: valueString)),
            const VerticalDivider(),
            Card(
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: bucketCounts.length,
                separatorBuilder: (context, index) {
                  return const VerticalDivider();
                },
                itemBuilder: (context, index) {
                  return SizedBox(
                    width: 100,
                    child: LabeledValueView(
                      label: bucketCounts[index].label,
                      value: bucketCounts[index].value,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class RequestsDurationViewItemLabel extends StatelessWidget {
  const RequestsDurationViewItemLabel({
    super.key,
    required this.path,
    required this.method,
    required this.status,
  });

  final String path;
  final String method;
  final String status;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              path,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            Divider(
              color: statusCodeToColor(int.parse(status)),
            ),
            Text(
              method,
              style: const TextStyle(fontSize: 16),
            ),
            Text(
              status,
              style: const TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}

class LabeledValueView extends StatelessWidget {
  const LabeledValueView({
    super.key,
    required this.label,
    required this.value,
  });

  final String label;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text(
              label,
              style: const TextStyle(fontSize: 16),
            ),
            const Divider(),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    value.toString(),
                    style: const TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
