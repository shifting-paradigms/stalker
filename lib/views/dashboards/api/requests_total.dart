import 'package:flutter/material.dart';

import 'package:insights_client/models/metrics/api.dart';
import 'package:insights_client/views/dashboards/utils.dart';

class RequestsTotalView extends StatelessWidget {
  const RequestsTotalView({
    super.key,
    required this.httpRequestsTotal,
  });

  final List<PrometheusMetric> httpRequestsTotal;

  @override
  Widget build(BuildContext context) {
    httpRequestsTotal.sort(
        (a, b) => ('${a.path}${a.count}').compareTo('${b.path}${b.count}'));
    return Column(
      children: [
        Row(
          children: const [
            Text('Requests Total', style: TextStyle(fontSize: 24)),
          ],
        ),
        const SizedBox(height: 10),
        Wrap(
          children: [
            ...httpRequestsTotal.map(
              (e) => SizedBox(
                width: 150,
                child: RequestsTotalViewItem(item: e),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class RequestsTotalViewItem extends StatelessWidget {
  const RequestsTotalViewItem({
    super.key,
    required this.item,
  });

  final PrometheusMetric item;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Text(
                  item.count.round().toString(),
                  style: const TextStyle(
                      fontSize: 32, fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 4),
                Text(item.method),
                const SizedBox(height: 4),
                Text(item.path),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                color: statusCodeToColor(item.status),
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(4),
                  bottomRight: Radius.circular(4),
                )),
            padding: const EdgeInsets.all(4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(item.status.toString()),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
