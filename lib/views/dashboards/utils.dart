import 'package:flutter/material.dart';

Color statusCodeToColor(int statusCode) {
  if (statusCode >= 200 && statusCode < 300) {
    return Colors.green;
  } else if (statusCode >= 300 && statusCode < 400) {
    return Colors.blue;
  } else if (statusCode >= 400 && statusCode < 500) {
    return Colors.orange;
  } else if (statusCode >= 500 && statusCode < 600) {
    return Colors.red;
  } else {
    return Colors.grey;
  }
}

Color logLevelToColor(String level) {
  switch (level) {
    case 'ERROR':
      return Colors.red;
    case 'WARN':
      return Colors.orange;
    case 'INFO':
      return Colors.green;
    case 'DEBUG':
      return Colors.blue;
    case 'TRACE':
      return Colors.purple;
    default:
      return Colors.black;
  }
}
