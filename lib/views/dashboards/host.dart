import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:insights_client/controller/backend.dart';
import 'package:insights_client/models/backend.dart';
import 'package:insights_client/models/metrics/host.dart';
import 'package:insights_client/models/socket/packages.dart';

import 'package:insights_client/views/socket.dart';
import 'package:percent_indicator/percent_indicator.dart';

class HostDashboardWrapper extends StatefulWidget {
  const HostDashboardWrapper(
      {super.key, required this.snapshot, required this.backendController});

  final AsyncSnapshot snapshot;
  final BackendController backendController;

  @override
  State<HostDashboardWrapper> createState() => _HostDashboardWrapperState();
}

class _HostDashboardWrapperState extends State<HostDashboardWrapper>
    with AutomaticKeepAliveClientMixin {
  HostMetrics? hostMetrics;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    final backendState = BackendStateExtension.fromConnectionState(
        widget.snapshot.connectionState);
    if (backendState == BackendState.disconnected) {
      widget.backendController
          .disconnect(widget.backendController.activeBackend);
    }
  }

  @override
  void didUpdateWidget(covariant HostDashboardWrapper oldWidget) {
    super.didUpdateWidget(oldWidget);
    final backendState = BackendStateExtension.fromConnectionState(
        widget.snapshot.connectionState);
    if (backendState == BackendState.disconnected) {
      widget.backendController
          .disconnect(widget.backendController.activeBackend);
    }
    if (widget.snapshot.hasData) {
      final package = MetricsPackage.fromJson(
        jsonDecode(widget.snapshot.data),
      );
      if (package.kind == PackageKind.hostMetrics) {
        hostMetrics = HostMetrics.fromJson(package.data);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return (hostMetrics != null)
        ? HostDashboard(hostMetrics: hostMetrics!)
        : const NoData();
  }
}

class HostDashboard extends StatelessWidget {
  const HostDashboard({super.key, required this.hostMetrics});

  final HostMetrics hostMetrics;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              width: 400,
              height: 150,
              child: HostDashboardHeader(hostMetrics: hostMetrics),
            ),
            SizedBox(
              width: 550,
              height: 200,
              child: CpuDashboard(cpuMetrics: hostMetrics.cpu),
            ),
            SizedBox(
              width: 550,
              height: 200,
              child: MemoryDashboard(memoryMetrics: hostMetrics.memory),
            ),
            SizedBox(
              width: 550,
              height: 200,
              child: DiskDashboard(diskMetrics: hostMetrics.disk),
            ),
            SizedBox(
              width: 400,
              height: 150,
              child: IODashboard(ioMetrics: hostMetrics.io),
            ),
          ],
        ),
      ),
    );
  }
}

class HostDashboardHeader extends StatelessWidget {
  const HostDashboardHeader({super.key, required this.hostMetrics});

  final HostMetrics hostMetrics;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Host Metrics',
                      style: TextStyle(
                        fontSize: 32,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(hostMetrics.name),
                  ],
                ),
              ),
              const Icon(Icons.data_usage, size: 80),
            ],
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Version: ${hostMetrics.osVersion}'),
                    Text('Kernel: ${hostMetrics.kernel}'),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text('Uptime: ${hostMetrics.uptime.inDays} days'),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CpuDashboard extends StatelessWidget {
  const CpuDashboard({super.key, required this.cpuMetrics});

  final CpuMetrics cpuMetrics;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    CircularUsageIndicator(percentage: cpuMetrics.usage / 100),
              ),
              Expanded(
                child: SizedBox(
                  width: 350,
                  height: 180,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  children: [
                                    const Text('CPU',
                                        style: TextStyle(
                                            fontSize: 32,
                                            fontWeight: FontWeight.bold)),
                                    Text('${cpuMetrics.cores} cores'),
                                  ],
                                ),
                                const Icon(Icons.speed, size: 80),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text(
                                  'Load',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(width: 8),
                                const Text(
                                  '-',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(width: 8),
                                Text(
                                  'One: ${cpuMetrics.avgLoadOne}',
                                  style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(width: 8),
                                Text(
                                  'Five: ${cpuMetrics.avgLoadFive}',
                                  style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(width: 8),
                                Text(
                                  'Fifteen: ${cpuMetrics.avgLoadFifteen}',
                                  style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Text(cpuMetrics.brand)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class CircularUsageIndicator extends StatelessWidget {
  const CircularUsageIndicator({
    super.key,
    required this.percentage,
  });

  final double percentage;

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
        radius: 80,
        lineWidth: 24,
        animation: true,
        animationDuration: 800,
        animateFromLastPercent: true,
        circularStrokeCap: CircularStrokeCap.round,
        backgroundColor: Colors.grey.shade700,
        progressColor: Color.lerp(Colors.green, Colors.red, percentage),
        center: Text(
          '${(percentage * 100).round()}%',
          style: const TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
        ),
        percent: percentage);
  }
}

String _toReadableSizeFromMegaBytes(int megaBytes) {
  if (megaBytes < 1024) {
    return '$megaBytes MB';
  } else if (megaBytes < 1024 * 1024) {
    return '${(megaBytes / 1024).toStringAsFixed(2)} GB';
  } else {
    return '${(megaBytes / 1024 / 1024).toStringAsFixed(2)} TB';
  }
}

String _toReadableSizeFromBytes(int bytes) {
  if (bytes < 1024) {
    return '$bytes B';
  } else if (bytes < 1024 * 1024) {
    return '${(bytes / 1024).toStringAsFixed(2)} KB';
  } else if (bytes < 1024 * 1024 * 1024) {
    return '${(bytes / 1024 / 1024).toStringAsFixed(2)} MB';
  } else {
    return '${(bytes / 1024 / 1024 / 1024).toStringAsFixed(2)} GB';
  }
}

class MemoryDashboard extends StatelessWidget {
  const MemoryDashboard({super.key, required this.memoryMetrics});

  final MemoryMetrics memoryMetrics;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircularUsageIndicator(
                    percentage: memoryMetrics.percentage),
              ),
              Expanded(
                child: SizedBox(
                  width: 300,
                  height: 180,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  children: const [
                                    Text('RAM',
                                        style: TextStyle(
                                            fontSize: 32,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                                const Icon(Icons.memory, size: 80),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                    'Total: ${_toReadableSizeFromMegaBytes(memoryMetrics.total)}',
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                                const SizedBox(width: 8),
                                Text(
                                    'Used: ${_toReadableSizeFromMegaBytes(memoryMetrics.used)}',
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                    'Free: ${_toReadableSizeFromMegaBytes(memoryMetrics.free)}'),
                                const SizedBox(width: 8),
                                Text(
                                    'Available: ${_toReadableSizeFromMegaBytes(memoryMetrics.available)}'),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DiskDashboard extends StatelessWidget {
  const DiskDashboard({super.key, required this.diskMetrics});

  final DiskMetrics diskMetrics;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    CircularUsageIndicator(percentage: diskMetrics.percentage),
              ),
              Expanded(
                child: SizedBox(
                  width: 300,
                  height: 180,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  children: const [
                                    Text('DISK',
                                        style: TextStyle(
                                            fontSize: 32,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                                const Icon(Icons.storage, size: 80),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                    'Total: ${_toReadableSizeFromMegaBytes(diskMetrics.total)}',
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                                const SizedBox(width: 8),
                                Text(
                                    'Used: ${_toReadableSizeFromMegaBytes(diskMetrics.used)}',
                                    style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class IODashboard extends StatelessWidget {
  const IODashboard({super.key, required this.ioMetrics});

  final IOMetrics ioMetrics;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Column(
                                  children: const [
                                    Text('Network',
                                        style: TextStyle(
                                            fontSize: 32,
                                            fontWeight: FontWeight.bold)),
                                  ],
                                ),
                                const Icon(Icons.import_export, size: 80),
                              ],
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Row(
                                      children: [
                                        const Icon(Icons.arrow_circle_down),
                                        const SizedBox(width: 8),
                                        Text(
                                          _toReadableSizeFromBytes(
                                              ioMetrics.received),
                                          style: const TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(width: 24),
                                    Row(
                                      children: [
                                        const Icon(Icons.arrow_circle_up),
                                        const SizedBox(width: 8),
                                        Text(
                                            _toReadableSizeFromBytes(
                                                ioMetrics.transmitted),
                                            style: const TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
