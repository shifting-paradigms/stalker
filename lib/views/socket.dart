import 'package:flutter/material.dart';
import 'package:insights_client/models/backend.dart';
import 'package:insights_client/controller/backend.dart';

class ConnectionControlView extends StatelessWidget {
  const ConnectionControlView({super.key, required this.backendController});

  final BackendController backendController;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        StateIndicator(
          state: backendController.activeBackend.state,
        ),
        const SizedBox(height: 20),
        ElevatedButton(
            onPressed: () => {
                  backendController.connect(backendController.activeBackend),
                },
            child: const Text('Connect'))
      ],
    );
  }
}

class StateIndicator extends StatelessWidget {
  const StateIndicator({super.key, required this.state});

  final BackendState state;

  @override
  Widget build(BuildContext context) {
    switch (state) {
      case BackendState.connected:
        return const Connected();
      case BackendState.disconnected:
        return const NotConnected();
      case BackendState.offline:
        return const Offline();
      case BackendState.invalid:
        return const InvalidSettings();
      default:
        return const Waiting();
    }
  }
}

class Waiting extends StatelessWidget {
  const Waiting({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: SizedBox(
        height: 50,
        width: 50,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      ),
    );
  }
}

class Connected extends StatelessWidget {
  const Connected({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(Icons.cloud_done, color: Colors.green),
    );
  }
}

class NotConnected extends StatelessWidget {
  const NotConnected({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(Icons.cloud_off),
    );
  }
}

class Offline extends StatelessWidget {
  const Offline({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(Icons.cloud_off, color: Colors.red),
    );
  }
}

class InvalidSettings extends StatelessWidget {
  const InvalidSettings({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(Icons.settings, color: Colors.red),
    );
  }
}

class NoData extends StatelessWidget {
  const NoData({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('No data'),
    );
  }
}

class Error extends StatelessWidget {
  const Error({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(Icons.error, color: Colors.red),
    );
  }
}

class Unknown extends StatelessWidget {
  const Unknown({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Icon(Icons.help, color: Colors.yellow),
    );
  }
}
