import 'package:flutter/material.dart';
import 'package:insights_client/controller/backend.dart';

class BackendSettings extends StatefulWidget {
  const BackendSettings({super.key, required this.backendController});

  final BackendController backendController;

  @override
  State<BackendSettings> createState() => _BackendSettingsState();
}

class _BackendSettingsState extends State<BackendSettings>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: Column(
        children: [
          BackendSettingsForm(
            backendController: widget.backendController,
          ),
        ],
      ),
    );
  }
}

class BackendSettingsForm extends StatefulWidget {
  const BackendSettingsForm({super.key, required this.backendController});

  final BackendController backendController;

  @override
  State<BackendSettingsForm> createState() => _BackendSettingsFormState();
}

const intervals = [1, 5, 10, 15, 30, 60, 1000];

class ConnectionSwitch extends StatelessWidget {
  const ConnectionSwitch({super.key, required this.backendController});

  final BackendController backendController;

  @override
  Widget build(BuildContext context) {
    return backendController.activeBackend.isConnected()
        ? ElevatedButton(
            onPressed: () {
              backendController.disconnect(backendController.activeBackend);
            },
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.red)),
            child: const Icon(Icons.stop_circle_outlined))
        : ElevatedButton(
            onPressed: () {
              backendController.connect(backendController.activeBackend);
            },
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.green)),
            child: const Icon(Icons.play_circle_outlined));
  }
}

class _BackendSettingsFormState extends State<BackendSettingsForm> {
  late TextEditingController nameController;
  late TextEditingController addressController;
  late TextEditingController apiKeyController;
  late int hostInterval;
  late int apiInterval;
  late int logInterval;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(
        text: widget.backendController.activeBackend.name);
    addressController = TextEditingController(
        text: widget.backendController.activeBackend.address);
    apiKeyController = TextEditingController(
        text: widget.backendController.activeBackend.apiKey);
    hostInterval =
        widget.backendController.activeBackend.streamConfig.host.interval;
    apiInterval =
        widget.backendController.activeBackend.streamConfig.api.interval;
    logInterval =
        widget.backendController.activeBackend.streamConfig.log.interval;
    setState(() {});
  }

  @override
  void didUpdateWidget(covariant BackendSettingsForm oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (!widget.backendController.activeBackend.isConnected()) {
      // If the active backend is not connected, we need to update the
      // widget content here in order to trigger a rebuild, while staying
      // on the settings screen.
      // If the active backend is connected, we must not update the widget
      // content here, because this would trigger a rebuild and the user
      // would be unable to change the settings.
      nameController = TextEditingController(
          text: widget.backendController.activeBackend.name);
      addressController = TextEditingController(
          text: widget.backendController.activeBackend.address);
      apiKeyController = TextEditingController(
          text: widget.backendController.activeBackend.apiKey);
      hostInterval =
          widget.backendController.activeBackend.streamConfig.host.interval;
      apiInterval =
          widget.backendController.activeBackend.streamConfig.api.interval;
      logInterval =
          widget.backendController.activeBackend.streamConfig.log.interval;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text('Backend', style: TextStyle(fontSize: 24.0)),
            Row(
              children: [
                ConnectionSwitch(backendController: widget.backendController),
                const SizedBox(width: 10.0),
                ElevatedButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: const Text('Remove Backend'),
                          content: const Text(
                              'Are you sure you want to delete this backend?'),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: const Text('Cancel'),
                            ),
                            TextButton(
                              onPressed: () {
                                widget.backendController.removeActiveBackend();
                                Navigator.of(context).pop();
                              },
                              child: const Text('Delete'),
                            ),
                          ],
                        );
                      },
                    );
                  },
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.grey),
                  ),
                  child: const Icon(Icons.delete),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(height: 20.0),
        TextField(
          controller: nameController,
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Name',
              hintText: 'Enter a name for the backend'),
        ),
        const SizedBox(height: 10.0),
        TextField(
          controller: addressController,
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Websocket Address',
              hintText: 'Enter an address to connect to'),
        ),
        const SizedBox(height: 10.0),
        TextField(
          controller: apiKeyController,
          decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Api Key',
              hintText: 'Enter an api key to authenticate with'),
        ),
        const SizedBox(height: 32.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            Text('Stream', style: TextStyle(fontSize: 24.0)),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const Icon(Icons.data_usage),
            const SizedBox(width: 10.0),
            SizedBox(
              width: 300.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Receive host metrics every:'),
                  DropdownButton<int>(
                      value: hostInterval,
                      items: intervals.map((int value) {
                        return DropdownMenuItem(
                          value: value,
                          child: Text('${value.toString()}s'),
                        );
                      }).toList(),
                      onChanged: (int? value) {
                        hostInterval = value!;
                        setState(() {});
                      }),
                ],
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const Icon(Icons.data_object),
            const SizedBox(width: 10.0),
            SizedBox(
              width: 300.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Receive api metrics every:'),
                  DropdownButton<int>(
                      value: apiInterval,
                      items: intervals.map((int value) {
                        return DropdownMenuItem(
                          value: value,
                          child: Text('${value.toString()}s'),
                        );
                      }).toList(),
                      onChanged: (int? value) {
                        apiInterval = value!;
                        setState(() {});
                      }),
                ],
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const Icon(Icons.speaker_notes),
            const SizedBox(width: 10.0),
            SizedBox(
              width: 300.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Receive log messages every:'),
                  DropdownButton<int>(
                      value: logInterval,
                      items: intervals.map((int value) {
                        return DropdownMenuItem(
                          value: value,
                          child: Text('${value.toString()}s'),
                        );
                      }).toList(),
                      onChanged: (int? value) {
                        logInterval = value!;
                        setState(() {});
                      }),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(height: 32.0),
        ElevatedButton(
          onPressed: () {
            var backend = widget.backendController.activeBackend;
            if (nameController.text.isNotEmpty) {
              backend.name = nameController.text;
            }
            if (addressController.text.isNotEmpty) {
              backend.address = addressController.text;
            }
            if (apiKeyController.text.isNotEmpty) {
              backend.apiKey = apiKeyController.text;
            }
            backend.setHostInterval(hostInterval);
            backend.setApiInterval(apiInterval);
            backend.setLogInterval(logInterval);

            widget.backendController.updateBackend(backend);
            setState(() {});
          },
          child: const Text('Save'),
        )
      ]),
    );
  }
}
