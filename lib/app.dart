import 'package:flutter/material.dart';
import 'controller/backend.dart';
import 'views/settings.dart';
import 'views/socket.dart';
import 'views/dashboards/host.dart';
import 'views/dashboards/api/prometheus.dart';
import 'views/dashboards/log/log.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Insights Client',
      theme: ThemeData.dark(),
      home: const App(),
    ),
  );
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with SingleTickerProviderStateMixin {
  late BackendController _backendController;
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this, initialIndex: 0);
    _backendController = BackendController(setAppState: setState);
    _backendController.initialize();
  }

  @override
  Widget build(BuildContext context) {
    if (_backendController.isLoading) {
      return const Waiting();
    } else {
      return Scaffold(
          appBar: AppBar(
            title: Text(_backendController.activeBackend.name),
            centerTitle: true,
            bottom: TabBar(
              controller: _tabController,
              tabs: const [
                Tab(icon: Icon(Icons.data_usage)),
                Tab(icon: Icon(Icons.data_object)),
                Tab(icon: Icon(Icons.speaker_notes)),
                Tab(icon: Icon(Icons.settings))
              ],
              indicatorColor: Colors.white,
            ),
          ),
          body: _backendController.activeBackend.isConnected()
              ? StreamBuilder(
                  stream: _backendController.activeBackend.getStream(),
                  builder: (context, snapshot) {
                    return TabBarView(controller: _tabController, children: [
                      HostDashboardWrapper(
                          snapshot: snapshot,
                          backendController: _backendController),
                      ApiDashboardWrapper(
                          snapshot: snapshot,
                          backendController: _backendController),
                      LogDashboardWrapper(
                          snapshot: snapshot,
                          backendController: _backendController),
                      BackendSettings(
                        backendController: _backendController,
                      )
                    ]);
                  })
              : TabBarView(
                  controller: _tabController,
                  children: [
                    ConnectionControlView(
                        backendController: _backendController),
                    ConnectionControlView(
                        backendController: _backendController),
                    ConnectionControlView(
                        backendController: _backendController),
                    BackendSettings(
                      backendController: _backendController,
                    )
                  ],
                ),
          drawer: Drawer(
              child: ListView(padding: EdgeInsets.zero, children: <Widget>[
            for (var backend in _backendController.registeredBackends)
              ListTile(
                selected: _backendController.isActive(backend),
                leading: const Icon(Icons.dns),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(backend.name),
                    StateIndicator(state: backend.state)
                  ],
                ),
                onTap: () => {
                  Navigator.pop(context),
                  _backendController.activate(backend),
                  _tabController.animateTo(0),
                },
              ),
            const SizedBox(height: 20),
            ElevatedButton(
                style:
                    ElevatedButton.styleFrom(padding: const EdgeInsets.all(20)),
                onPressed: () => {
                      Navigator.pop(context),
                      _backendController.createNewBackend(),
                      _tabController.animateTo(3),
                    },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [Icon(Icons.add), Text('Add Backend')],
                ))
          ])));
    }
  }
}
